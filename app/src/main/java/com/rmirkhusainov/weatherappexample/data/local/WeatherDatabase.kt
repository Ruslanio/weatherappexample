package com.rmirkhusainov.weatherappexample.data.local

import androidx.room.Database
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.rmirkhusainov.weatherappexample.data.local.converters.WeatherTypeConverter
import com.rmirkhusainov.weatherappexample.data.local.dao.WeatherDao
import com.rmirkhusainov.weatherappexample.data.local.model.UserLocation
import com.rmirkhusainov.weatherappexample.data.local.model.WeatherForecast

@Database(
    entities = [UserLocation::class, WeatherForecast::class],
    version = 1
)
@TypeConverters(WeatherTypeConverter::class)
abstract class WeatherDatabase : RoomDatabase() {

    abstract fun userLocationsDao(): WeatherDao
}