package com.rmirkhusainov.weatherappexample.data.local.model

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user_location")
data class UserLocation(
    @PrimaryKey
    @ColumnInfo(name = PRIMARY_KEY)
    val locationId: Int,
    val name: String
) {
    companion object {
        const val PRIMARY_KEY = "locationId"
    }
}