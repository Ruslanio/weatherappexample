package com.rmirkhusainov.weatherappexample.data.repositories.impl

import com.rmirkhusainov.weatherappexample.data.local.dao.WeatherDao
import com.rmirkhusainov.weatherappexample.data.local.model.LocationWithForecasts
import com.rmirkhusainov.weatherappexample.data.local.model.UserLocation
import com.rmirkhusainov.weatherappexample.data.local.model.WeatherForecast
import com.rmirkhusainov.weatherappexample.data.repositories.LocationRepository
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single
import javax.inject.Inject

class LocationRepositoryImpl @Inject constructor(
    private val weatherDao: WeatherDao
) : LocationRepository {

    override fun getAllLocations(): Single<List<UserLocation>> {
        return weatherDao.queryForAll()
    }

    override fun getLocationWithForecasts(locationId: Int): Single<LocationWithForecasts> {
        return weatherDao.queryByIdWithForecasts(locationId)
    }

    override fun saveLocation(location: UserLocation): Completable {
        return weatherDao.saveLocation(location)
    }

    override fun saveLocationsWithForecastList(locationWithForecasts: List<LocationWithForecasts>): Completable {
        return Completable.concat(
            locationWithForecasts.map {
                weatherDao.save(it)
            }
        )
    }

    override fun getForecastById(forecastId: Int): Single<WeatherForecast> {
        return weatherDao.queryForecastById(forecastId)
    }

    override fun saveLocationsWithForecast(locationWithForecasts: LocationWithForecasts): Completable {
        return weatherDao.save(locationWithForecasts)
    }
}