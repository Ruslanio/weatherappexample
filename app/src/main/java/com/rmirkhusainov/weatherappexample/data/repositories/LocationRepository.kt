package com.rmirkhusainov.weatherappexample.data.repositories

import com.rmirkhusainov.weatherappexample.data.local.model.LocationWithForecasts
import com.rmirkhusainov.weatherappexample.data.local.model.UserLocation
import com.rmirkhusainov.weatherappexample.data.local.model.WeatherForecast
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

interface LocationRepository {

    fun getAllLocations(): Single<List<UserLocation>>

    fun getForecastById(forecastId: Int): Single<WeatherForecast>

    fun saveLocation(location: UserLocation): Completable

    fun getLocationWithForecasts(locationId: Int): Single<LocationWithForecasts>

    fun saveLocationsWithForecast(locationWithForecasts: LocationWithForecasts): Completable

    fun saveLocationsWithForecastList(locationWithForecasts: List<LocationWithForecasts>): Completable
}