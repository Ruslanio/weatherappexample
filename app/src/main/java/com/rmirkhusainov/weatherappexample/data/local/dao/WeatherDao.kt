package com.rmirkhusainov.weatherappexample.data.local.dao

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy.REPLACE
import androidx.room.Query
import androidx.room.Transaction
import com.rmirkhusainov.weatherappexample.data.local.model.LocationWithForecasts
import com.rmirkhusainov.weatherappexample.data.local.model.UserLocation
import com.rmirkhusainov.weatherappexample.data.local.model.WeatherForecast
import io.reactivex.rxjava3.core.Completable
import io.reactivex.rxjava3.core.Single

@Dao
abstract class WeatherDao {

    @Query("SELECT * FROM user_location")
    abstract fun queryForAll(): Single<List<UserLocation>>

    @Transaction
    @Query("SELECT * FROM user_location WHERE locationId = :locationId")
    abstract fun queryByIdWithForecasts(locationId: Int): Single<LocationWithForecasts>

    @Query("SELECT * FROM weather_forecast WHERE forecastId = :forecastID")
    abstract fun queryForecastById(forecastID: Int): Single<WeatherForecast>

    @Insert(onConflict = REPLACE)
    abstract fun saveLocation(userLocation: UserLocation): Completable

    @Insert(onConflict = REPLACE)
    abstract fun saveForecasts(weatherForecast: List<WeatherForecast>): Completable

    fun save(locationWithForecasts: LocationWithForecasts) =
        saveLocation(locationWithForecasts.userLocation)
            .andThen(saveForecasts(locationWithForecasts.weatherForecastList))
}