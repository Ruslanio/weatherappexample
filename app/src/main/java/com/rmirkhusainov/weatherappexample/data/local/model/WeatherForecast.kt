package com.rmirkhusainov.weatherappexample.data.local.model

import androidx.annotation.DrawableRes
import androidx.room.ColumnInfo
import androidx.room.Embedded
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.rmirkhusainov.weatherappexample.R

@Entity(tableName = "weather_forecast")
data class WeatherForecast(
    @PrimaryKey
    @ColumnInfo(name = PRIMARY_KEY)
    val forecastId: Int,
    @ColumnInfo(name = FOREIGN_KEY_LOCATION)
    val userLocationId: Int,
    @Embedded
    val temperature: Temperature,
    val weatherType: WeatherType,
    val date: String,
    val humidity: Int,
    val pressure: Int,
    val uvIndex: Int,
    val sunriseTime: String,
    val sunsetTime: String
) {
    companion object {
        const val PRIMARY_KEY = "forecastId"
        const val FOREIGN_KEY_LOCATION = "userLocationId"
    }
}

data class Temperature(
    val day: Int,
    val night: Int,
    val feelsLike: Int
)

enum class WeatherType(@DrawableRes val imageId: Int) {
    RAIN(R.drawable.ic_weather_rain),
    SUNNY(R.drawable.ic_weather_sunny),
    CLOUDED(R.drawable.ic_weather_clouded)
}