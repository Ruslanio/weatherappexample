package com.rmirkhusainov.weatherappexample.data.local.model

import androidx.room.Embedded
import androidx.room.Relation

data class LocationWithForecasts(
    @Embedded
    val userLocation: UserLocation,
    @Relation(
        parentColumn = UserLocation.PRIMARY_KEY,
        entityColumn = WeatherForecast.FOREIGN_KEY_LOCATION
    )
    val weatherForecastList: List<WeatherForecast>
)