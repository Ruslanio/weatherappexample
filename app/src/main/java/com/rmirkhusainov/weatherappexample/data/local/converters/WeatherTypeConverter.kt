package com.rmirkhusainov.weatherappexample.data.local.converters

import androidx.room.TypeConverter
import com.rmirkhusainov.weatherappexample.data.local.model.WeatherType

class WeatherTypeConverter {

    @TypeConverter
    fun toType(ordinal: Int): WeatherType {
        return enumValues<WeatherType>()[ordinal]
    }

    @TypeConverter
    fun toOrdinal(type: WeatherType): Int {
        return type.ordinal
    }
}