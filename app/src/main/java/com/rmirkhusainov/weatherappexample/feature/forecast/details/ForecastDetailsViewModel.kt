package com.rmirkhusainov.weatherappexample.feature.forecast.details

import com.rmirkhusainov.weatherappexample.R
import com.rmirkhusainov.weatherappexample.core.ext.disposeBy
import com.rmirkhusainov.weatherappexample.core.ext.subscribeByDefault
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseViewModel
import com.rmirkhusainov.weatherappexample.data.repositories.LocationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ForecastDetailsViewModel @Inject constructor(
    private val locationRepository: LocationRepository
) : BaseViewModel<ForecastDetailsState>() {
    override fun getInitialState() = ForecastDetailsState.Loading

    fun initForecast(forecastId: Int) {
        locationRepository.getForecastById(forecastId)
            .doOnSubscribe { uiState.postValue(ForecastDetailsState.Loading) }
            .subscribeByDefault()
            .subscribe(
                {
                    uiState.postValue(ForecastDetailsState.HasData(it))
                },
                {
                    uiState.postValue(ForecastDetailsState.Error(R.string.common_error))
                }
            )
            .disposeBy(bag)
    }
}