package com.rmirkhusainov.weatherappexample.feature.locations.creation

import android.os.Bundle
import com.rmirkhusainov.weatherappexample.core.ext.setNavigationResult
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseMvvmDialog
import com.rmirkhusainov.weatherappexample.core.utils.Event
import com.rmirkhusainov.weatherappexample.databinding.DialogLocationCreationBinding

class LocationCreationDialogFragment : BaseMvvmDialog<DialogLocationCreationBinding>() {

    override fun initBinding() = DialogLocationCreationBinding.inflate(layoutInflater)

    override fun onInit(savedInstanceState: Bundle?) {
        binding?.apply {
            btnSave.setOnClickListener {
                val name = inputLocationName.text.toString()
                if (name.isNotEmpty() && name.isNotBlank()) {
                    setNavigationResult(Event(inputLocationName.text.toString()))
                    dismiss()
                }
            }
            btnCancel.setOnClickListener {
                dismiss()
            }
        }
    }
}