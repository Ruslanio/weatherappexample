package com.rmirkhusainov.weatherappexample.feature.forecast.list

import com.rmirkhusainov.weatherappexample.R
import com.rmirkhusainov.weatherappexample.core.ext.disposeBy
import com.rmirkhusainov.weatherappexample.core.ext.subscribeByDefault
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseViewModel
import com.rmirkhusainov.weatherappexample.data.repositories.LocationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class ForecastLisViewModel @Inject constructor(
    private val locationRepository: LocationRepository
) : BaseViewModel<ForecastListState>() {
    override fun getInitialState() = ForecastListState.Loading

    fun initForecasts(locationId: Int) {
        locationRepository.getLocationWithForecasts(locationId)
            .doOnSubscribe { uiState.postValue(ForecastListState.Loading) }
            .subscribeByDefault()
            .subscribe(
                {
                    uiState.postValue(
                        ForecastListState.HasData(
                            it.userLocation.name,
                            it.weatherForecastList
                        )
                    )
                },
                {
                    uiState.postValue(ForecastListState.Error(R.string.common_error))
                }
            )
            .disposeBy(bag)
    }
}