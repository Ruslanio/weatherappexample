package com.rmirkhusainov.weatherappexample.feature.locations

import com.rmirkhusainov.weatherappexample.R
import com.rmirkhusainov.weatherappexample.core.ext.disposeBy
import com.rmirkhusainov.weatherappexample.core.ext.subscribeByDefault
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseViewModel
import com.rmirkhusainov.weatherappexample.core.utils.FakeDataManager
import com.rmirkhusainov.weatherappexample.data.local.model.LocationWithForecasts
import com.rmirkhusainov.weatherappexample.data.repositories.LocationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class LocationsViewModel @Inject constructor(
    private val locationRepository: LocationRepository
) : BaseViewModel<LocationState>() {
    override fun getInitialState() = LocationState.Loading

    fun initLocationList() {
        locationRepository.getAllLocations()
            .doOnSubscribe { uiState.postValue(LocationState.Loading) }
            .subscribeByDefault()
            .subscribe({
                uiState.postValue(LocationState.HasData(it))
            }, {
                uiState.postValue(LocationState.Error(R.string.common_error))
            })
            .disposeBy(bag)
    }

    fun addLocation(name: String) {
        val location = FakeDataManager.createLocationByName(name)
        locationRepository.saveLocationsWithForecast(
            LocationWithForecasts(
                location,
                FakeDataManager.createForecastList(location)
            )
        ).andThen(locationRepository.getAllLocations())
            .doOnSubscribe { uiState.postValue(LocationState.Loading) }
            .subscribeByDefault()
            .subscribe({
                uiState.postValue(LocationState.HasData(it))
            }, {
                uiState.postValue(LocationState.Error(R.string.common_error))
            })
            .disposeBy(bag)
    }
}