package com.rmirkhusainov.weatherappexample.feature.forecast.details

import androidx.annotation.StringRes
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseState
import com.rmirkhusainov.weatherappexample.data.local.model.WeatherForecast

sealed class ForecastDetailsState : BaseState {
    object Loading : ForecastDetailsState()
    data class HasData(val weatherForecast: WeatherForecast) : ForecastDetailsState()
    data class Error(@StringRes val messageId: Int) : ForecastDetailsState()
}
