package com.rmirkhusainov.weatherappexample.feature.forecast.list

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import androidx.recyclerview.widget.LinearLayoutManager
import com.rmirkhusainov.weatherappexample.core.ext.makeGone
import com.rmirkhusainov.weatherappexample.core.ext.makeVisible
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseMvvmFragment
import com.rmirkhusainov.weatherappexample.data.local.model.WeatherForecast
import com.rmirkhusainov.weatherappexample.databinding.FragmentForecastListBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForecastListFragment
    : BaseMvvmFragment<FragmentForecastListBinding, ForecastListState, ForecastLisViewModel>() {

    override fun initBinding() = FragmentForecastListBinding.inflate(layoutInflater)

    override val viewModel by viewModels<ForecastLisViewModel>()
    private val adapter = ForecastsAdapter(::showDetails)
    private val args by navArgs<ForecastListFragmentArgs>()


    override fun onInit(savedInstanceState: Bundle?) {

        binding?.apply {
            rvForecasts.layoutManager = LinearLayoutManager(requireContext())
            rvForecasts.adapter = adapter
        }
        viewModel.initForecasts(args.locationId)
    }

    private fun showDetails(forecastId: Int) {
        findNavController().navigate(
            ForecastListFragmentDirections.actionForecastListFragmentToForecastDetailsFragment(
                forecastId
            )
        )
    }

    override fun render(state: ForecastListState) {
        when (state) {
            ForecastListState.Loading -> showLoading()
            is ForecastListState.HasData -> showForecasts(state.locationName, state.forecasts)
            is ForecastListState.Error -> showError(state.messageId)
        }
    }

    private fun showError(messageId: Int) = binding?.apply {
        progress.makeGone()
        rvForecasts.makeGone()
        tvLocationName.makeGone()

        tvError.text = getString(messageId)
        tvError.makeVisible()
    }

    private fun showLoading() = binding?.apply {
        rvForecasts.makeGone()
        tvError.makeGone()
        tvLocationName.makeGone()
        progress.makeVisible()
    }

    private fun showForecasts(locationName: String, forecasts: List<WeatherForecast>) =
        binding?.apply {
            rvForecasts.makeVisible()
            tvLocationName.makeVisible()
            tvError.makeGone()
            progress.makeGone()

            tvLocationName.text = locationName
            adapter.setItems(forecasts)
        }
}