package com.rmirkhusainov.weatherappexample.feature.main

import android.util.Log
import com.rmirkhusainov.weatherappexample.core.ext.disposeBy
import com.rmirkhusainov.weatherappexample.core.ext.subscribeByDefault
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseViewModel
import com.rmirkhusainov.weatherappexample.core.utils.FakeDataManager
import com.rmirkhusainov.weatherappexample.data.repositories.LocationRepository
import dagger.hilt.android.lifecycle.HiltViewModel
import javax.inject.Inject

@HiltViewModel
class MainViewModel @Inject constructor(
    private val locationRepository: LocationRepository
) : BaseViewModel<MainState>() {

    override fun getInitialState() = MainState.Loading

    fun populateDb() {
        locationRepository.saveLocationsWithForecastList(FakeDataManager.createUserLocationList())
            .doOnSubscribe { uiState.postValue(MainState.Loading) }
            .subscribeByDefault()
            .subscribe(
                {
                    uiState.postValue(MainState.Initialized)
                },
                {
                    Log.e("TAG", it.message, it)
                }
            )
            .disposeBy(bag)
    }

}