package com.rmirkhusainov.weatherappexample.feature.main

import android.os.Bundle
import androidx.activity.viewModels
import androidx.navigation.findNavController
import com.rmirkhusainov.weatherappexample.R
import com.rmirkhusainov.weatherappexample.core.ext.makeGone
import com.rmirkhusainov.weatherappexample.core.ext.makeVisible
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseMvvmActivity
import com.rmirkhusainov.weatherappexample.databinding.ActivityMainBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class MainActivity : BaseMvvmActivity<ActivityMainBinding, MainState, MainViewModel>() {

    override fun initBinding() = ActivityMainBinding.inflate(layoutInflater)

    override val viewModel by viewModels<MainViewModel>()

    override fun onInit(savedInstanceState: Bundle?) {
        viewModel.populateDb()
    }

    override fun render(state: MainState) {
        when (state) {
            MainState.Loading -> showLoading()
            MainState.Initialized -> showContainer()
        }
    }

    private fun showContainer() = with(binding) {
        progress.makeGone()
        findNavController(R.id.nav_host_container).setGraph(R.navigation.navigation_main)
    }

    private fun showLoading() = with(binding) {
        progress.makeVisible()
    }

}