package com.rmirkhusainov.weatherappexample.feature.forecast.list

import androidx.annotation.StringRes
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseState
import com.rmirkhusainov.weatherappexample.data.local.model.WeatherForecast

sealed class ForecastListState : BaseState {
    object Loading : ForecastListState()
    data class HasData(val locationName: String, val forecasts: List<WeatherForecast>) :
        ForecastListState()

    data class Error(@StringRes val messageId: Int) : ForecastListState()
}
