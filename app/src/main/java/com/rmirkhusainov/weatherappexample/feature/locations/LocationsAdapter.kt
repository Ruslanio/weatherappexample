package com.rmirkhusainov.weatherappexample.feature.locations

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rmirkhusainov.weatherappexample.data.local.model.UserLocation
import com.rmirkhusainov.weatherappexample.databinding.ItemLocationBinding

typealias OnLocationClickListener = (locationId: Int) -> Unit

class LocationsAdapter(
    private val onLocationClickListener: OnLocationClickListener
) : RecyclerView.Adapter<LocationViewHolder>() {

    private val items: MutableList<UserLocation> = mutableListOf()

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(newItems: List<UserLocation>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    fun addItem(location: UserLocation) {
        items.add(location)
        notifyItemInserted(items.size - 1)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): LocationViewHolder {
        val binding = ItemLocationBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return LocationViewHolder(binding, onLocationClickListener)
    }

    override fun onBindViewHolder(holder: LocationViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size
}

class LocationViewHolder(
    private val binding: ItemLocationBinding,
    private val onLocationClickListener: OnLocationClickListener
) :
    RecyclerView.ViewHolder(binding.root) {

    fun bind(location: UserLocation) = with(binding) {
        containerLocation.setOnClickListener {
            onLocationClickListener.invoke(location.locationId)
        }
        tvLocationName.text = location.name

    }
}