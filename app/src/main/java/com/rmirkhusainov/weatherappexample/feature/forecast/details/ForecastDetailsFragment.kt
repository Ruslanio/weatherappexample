package com.rmirkhusainov.weatherappexample.feature.forecast.details

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.navArgs
import com.rmirkhusainov.weatherappexample.R
import com.rmirkhusainov.weatherappexample.core.ext.makeGone
import com.rmirkhusainov.weatherappexample.core.ext.makeVisible
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseMvvmFragment
import com.rmirkhusainov.weatherappexample.data.local.model.WeatherForecast
import com.rmirkhusainov.weatherappexample.databinding.FragmentForecastDetailsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class ForecastDetailsFragment
    :
    BaseMvvmFragment<FragmentForecastDetailsBinding, ForecastDetailsState, ForecastDetailsViewModel>() {

    override fun initBinding() = FragmentForecastDetailsBinding.inflate(layoutInflater)

    override val viewModel by viewModels<ForecastDetailsViewModel>()
    private val args by navArgs<ForecastDetailsFragmentArgs>()

    override fun onInit(savedInstanceState: Bundle?) {
        viewModel.initForecast(args.forecastId)
    }

    override fun render(state: ForecastDetailsState) {
        when (state) {
            is ForecastDetailsState.HasData -> showData(state.weatherForecast)
            ForecastDetailsState.Loading -> showLoading()
            is ForecastDetailsState.Error -> showError(state.messageId)
        }
    }

    private fun showError(messageId: Int) = binding?.apply {
        progress.makeGone()
        containerDetails.makeGone()

        tvError.text = getString(messageId)
        tvError.makeVisible()
    }

    private fun showLoading() = binding?.apply {
        containerDetails.makeGone()
        tvError.makeGone()
        progress.makeVisible()
    }

    private fun showData(forecast: WeatherForecast) = binding?.apply {
        containerDetails.makeVisible()
        progress.makeGone()
        tvError.makeGone()

        ivWeatherType.setImageResource(forecast.weatherType.imageId)
        tvTempValue.text = getString(
            R.string.temp_overall,
            forecast.temperature.day,
            forecast.temperature.night,
            forecast.temperature.feelsLike
        )
        tvHumidityValue.text = getString(R.string.humidity_value, forecast.humidity)
        tvPressureValue.text = getString(R.string.pressure_value,forecast.pressure)
        tvUVValue.text = forecast.uvIndex.toString()
        tvSunriseValue.text = forecast.sunriseTime
        tvSunsetValue.text = forecast.sunsetTime
    }
}