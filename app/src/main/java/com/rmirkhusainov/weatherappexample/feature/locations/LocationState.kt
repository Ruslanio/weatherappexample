package com.rmirkhusainov.weatherappexample.feature.locations

import androidx.annotation.StringRes
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseState
import com.rmirkhusainov.weatherappexample.data.local.model.UserLocation

sealed class LocationState : BaseState {

    object Loading : LocationState()
    data class HasData(val locations: List<UserLocation>) : LocationState()
    data class Error(@StringRes val messageId: Int) : LocationState()
}