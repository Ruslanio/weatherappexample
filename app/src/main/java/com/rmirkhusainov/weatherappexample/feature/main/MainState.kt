package com.rmirkhusainov.weatherappexample.feature.main

import com.rmirkhusainov.weatherappexample.core.mvvm.BaseState

sealed class MainState : BaseState {
    object Loading : MainState()
    object Initialized : MainState()
}