package com.rmirkhusainov.weatherappexample.feature.locations

import android.os.Bundle
import androidx.fragment.app.viewModels
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.rmirkhusainov.weatherappexample.core.ext.getNavigationResult
import com.rmirkhusainov.weatherappexample.core.ext.makeGone
import com.rmirkhusainov.weatherappexample.core.ext.makeVisible
import com.rmirkhusainov.weatherappexample.core.mvvm.BaseMvvmFragment
import com.rmirkhusainov.weatherappexample.core.utils.Event
import com.rmirkhusainov.weatherappexample.data.local.model.UserLocation
import com.rmirkhusainov.weatherappexample.databinding.FragmentLocationsBinding
import dagger.hilt.android.AndroidEntryPoint

@AndroidEntryPoint
class LocationsFragment :
    BaseMvvmFragment<FragmentLocationsBinding, LocationState, LocationsViewModel>() {
    override fun initBinding() = FragmentLocationsBinding.inflate(layoutInflater)

    override val viewModel by viewModels<LocationsViewModel>()

    private var adapter = LocationsAdapter(::onLocationClick)

    override fun onInit(savedInstanceState: Bundle?) {
        binding?.apply {
            fabCreateLocation.setOnClickListener {
                findNavController().navigate(
                    LocationsFragmentDirections.actionLocationsFragmentToLocationCreationDialog()
                )

            }
            rvLocations.layoutManager = LinearLayoutManager(requireContext())
            rvLocations.adapter = adapter
        }

        getNavigationResult<Event<String>>()?.observe(viewLifecycleOwner) { event ->
            event.getContentIfNotHandled()?.let {
                viewModel.addLocation(it)
            }
        }

        viewModel.initLocationList()
    }

    private fun onLocationClick(locationId: Int) {
        findNavController().navigate(
            LocationsFragmentDirections.actionLocationsFragmentToForecastListFragment(
                locationId
            )
        )
    }

    override fun render(state: LocationState) {
        when (state) {
            LocationState.Loading -> showLoading()
            is LocationState.HasData -> showData(state.locations)
            is LocationState.Error -> showError(state.messageId)
        }
    }

    private fun showError(messageId: Int) = binding?.apply {
        progress.makeGone()
        rvLocations.makeGone()

        tvError.text = getString(messageId)
        tvError.makeVisible()
    }

    private fun showData(locations: List<UserLocation>) = binding?.apply {
        rvLocations.makeVisible()
        tvError.makeGone()
        progress.makeGone()

        adapter.setItems(locations)
    }

    private fun showLoading() = binding?.apply {
        progress.makeVisible()
        rvLocations.makeGone()
        tvError.makeGone()
    }
}