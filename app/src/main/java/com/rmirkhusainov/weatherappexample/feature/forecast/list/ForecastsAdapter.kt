package com.rmirkhusainov.weatherappexample.feature.forecast.list

import android.annotation.SuppressLint
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.rmirkhusainov.weatherappexample.R
import com.rmirkhusainov.weatherappexample.data.local.model.WeatherForecast
import com.rmirkhusainov.weatherappexample.databinding.ItemWeatherForecastBinding

typealias OnForecastClickListener = (forecastId: Int) -> Unit

class ForecastsAdapter(
    private val onForecastClickListener: OnForecastClickListener
) : RecyclerView.Adapter<ForecastViewHolder>() {

    private val items = mutableListOf<WeatherForecast>()

    @SuppressLint("NotifyDataSetChanged")
    fun setItems(newItems: List<WeatherForecast>) {
        items.clear()
        items.addAll(newItems)
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ForecastViewHolder {
        val binding = ItemWeatherForecastBinding.inflate(
            LayoutInflater.from(parent.context), parent, false
        )
        return ForecastViewHolder(binding, onForecastClickListener)
    }

    override fun onBindViewHolder(holder: ForecastViewHolder, position: Int) {
        holder.bind(items[position])
    }

    override fun getItemCount() = items.size
}

class ForecastViewHolder(
    private val binding: ItemWeatherForecastBinding,
    private val onForecastClickListener: OnForecastClickListener
) : RecyclerView.ViewHolder(binding.root) {

    fun bind(weatherForecast: WeatherForecast) = with(binding) {
        tvForecastDate.text = weatherForecast.date
        tvForecastTemp.text = binding.root.context.getString(
            R.string.temp_day_night,
            weatherForecast.temperature.day,
            weatherForecast.temperature.night
        )
        ivWeatherType.setImageResource(weatherForecast.weatherType.imageId)
        containerForecast.setOnClickListener {
            onForecastClickListener.invoke(weatherForecast.forecastId)
        }
    }

}