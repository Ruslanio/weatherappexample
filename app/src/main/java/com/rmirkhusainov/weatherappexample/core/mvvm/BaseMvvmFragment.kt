package com.rmirkhusainov.weatherappexample.core.mvvm

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.viewbinding.ViewBinding

abstract class BaseMvvmFragment<VB : ViewBinding, S : BaseState, VM : BaseViewModel<S>>
    : Fragment(), BaseMvvmView<VB> {

    protected abstract val viewModel: VM

    protected var binding: VB? = null

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = initBinding()
        return binding?.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        onInit(savedInstanceState)
        viewModel.publicUiState.observe(viewLifecycleOwner, ::render)
    }

    protected abstract fun render(state: S)

    override fun onDestroyView() {
        super.onDestroyView()
        binding = null
    }
}