package com.rmirkhusainov.weatherappexample.core.di

import com.rmirkhusainov.weatherappexample.data.local.dao.WeatherDao
import com.rmirkhusainov.weatherappexample.data.repositories.LocationRepository
import com.rmirkhusainov.weatherappexample.data.repositories.impl.LocationRepositoryImpl
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object RepositoryModule {

    @Provides
    @Singleton
    fun provideLocationRepository(
        weatherDao: WeatherDao
    ): LocationRepository {
        return LocationRepositoryImpl(weatherDao)
    }

}