package com.rmirkhusainov.weatherappexample.core.ext

import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController

private const val TAG_RESULT = "result"

fun <T> Fragment.getNavigationResult(key: String = TAG_RESULT) =
    findNavController().currentBackStackEntry?.savedStateHandle?.getLiveData<T>(key)

fun <T> Fragment.setNavigationResult(result: T, key: String = TAG_RESULT) {
    findNavController().previousBackStackEntry?.savedStateHandle?.set(key, result)
}