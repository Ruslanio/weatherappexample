package com.rmirkhusainov.weatherappexample.core.mvvm

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.rxjava3.disposables.CompositeDisposable

abstract class BaseViewModel<S : BaseState> : ViewModel() {

    protected val bag = CompositeDisposable()

    protected val uiState by lazy { MutableLiveData(getInitialState()) }
    val publicUiState: LiveData<S> = uiState

    protected abstract fun getInitialState(): S

    override fun onCleared() {
        super.onCleared()
        bag.dispose()
    }
}