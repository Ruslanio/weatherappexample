package com.rmirkhusainov.weatherappexample.core.utils

import com.rmirkhusainov.weatherappexample.data.local.model.*
import java.time.LocalDate
import java.time.format.DateTimeFormatter
import java.time.format.FormatStyle
import kotlin.random.Random
import kotlin.random.nextInt

object FakeDataManager {

    private val today by lazy { LocalDate.now() }
    private val random by lazy { Random(53728) }

    private val locationNames = listOf(
        "Krakow",
        "Warsaw",
        "Lodz",
        "Gdansk",
        "Kazan",
        "Berlin",
        "London"
    )
    private var locationIdCount = locationNames.size - 1

    fun createLocationByName(locationName: String): UserLocation {
        return UserLocation(
            locationId = ++locationIdCount,
            name = locationName
        )
    }

    fun createUserLocationList(): List<LocationWithForecasts> {
        var location: UserLocation
        return locationNames.mapIndexed { index, name ->
            location = UserLocation(index, name)
            LocationWithForecasts(
                userLocation = location,
                weatherForecastList = createForecastList(location)
            )
        }
    }

    fun createForecastList(userLocation: UserLocation): List<WeatherForecast> {
        return mutableListOf<WeatherForecast>().apply {
            for (i in 0 until 7) {
                add(
                    WeatherForecast(
                        forecastId = "${userLocation.locationId}$i".toInt(),
                        userLocationId = userLocation.locationId,
                        temperature = Temperature(
                            day = getRandomTemp(),
                            night = getRandomTemp(),
                            feelsLike = getRandomTemp()
                        ),
                        weatherType = getRandomWeatherType(),
                        date = getDateString(today.plusDays(i.toLong())),
                        humidity = getRandomHumidity(),
                        pressure = getRandomPressure(),
                        uvIndex = getRandomUV(),
                        sunriseTime = "6:50",
                        sunsetTime = "21:13"
                    )
                )
            }
        }
    }

    private fun getRandomTemp(): Int {
        return random.nextInt(IntRange(-20, 20))
    }

    private fun getRandomHumidity(): Int {
        return random.nextInt(IntRange(10, 100))
    }

    private fun getRandomPressure(): Int {
        return random.nextInt(IntRange(900, 1100))
    }

    private fun getRandomUV(): Int {
        return random.nextInt(IntRange(0, 11))
    }

    private fun getRandomWeatherType(): WeatherType {
        return WeatherType.values()[random.nextInt(3)]
    }

    private fun getDateString(date: LocalDate) =
        date.format(DateTimeFormatter.ofLocalizedDate(FormatStyle.MEDIUM))

}