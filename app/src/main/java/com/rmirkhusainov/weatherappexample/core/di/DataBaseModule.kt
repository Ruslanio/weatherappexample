package com.rmirkhusainov.weatherappexample.core.di

import android.content.Context
import androidx.room.Room
import com.rmirkhusainov.weatherappexample.data.local.WeatherDatabase
import com.rmirkhusainov.weatherappexample.data.local.dao.WeatherDao
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@InstallIn(SingletonComponent::class)
@Module
object DataBaseModule {

    @Provides
    @Singleton
    fun provideDatabase(
        @ApplicationContext context: Context
    ): WeatherDatabase {
        return Room.databaseBuilder(
            context,
            WeatherDatabase::class.java,
            "weather.db"
        ).build()
    }

    @Provides
    @Singleton
    fun provideLocationsDao(database: WeatherDatabase): WeatherDao {
        return database.userLocationsDao()
    }
}