package com.rmirkhusainov.weatherappexample.core.mvvm

import android.os.Bundle
import androidx.viewbinding.ViewBinding

interface BaseMvvmView<VB : ViewBinding> {

    fun initBinding(): VB

    fun onInit(savedInstanceState: Bundle?)
}