package com.rmirkhusainov.weatherappexample.core.mvvm

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.viewbinding.ViewBinding

abstract class BaseMvvmActivity<VB : ViewBinding, S : BaseState, VM : BaseViewModel<S>> :
    AppCompatActivity(), BaseMvvmView<VB> {

    protected lateinit var binding: VB

    protected abstract val viewModel: VM

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = initBinding()
        setContentView(binding.root)
        onInit(savedInstanceState)
        viewModel.publicUiState.observe(this, ::render)
    }

    protected abstract fun render(state: S)

}