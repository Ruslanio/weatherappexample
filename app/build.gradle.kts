plugins {
    id("com.android.application")
    id("org.jetbrains.kotlin.android")
    kotlin("android.extensions")
    id("androidx.navigation.safeargs")
    kotlin("kapt")
    id("dagger.hilt.android.plugin")
}

android {
    compileSdkVersion(ConfigData.compileSdkVersion)
    buildToolsVersion = ConfigData.buildToolsVersion

    defaultConfig {
        applicationId = ConfigData.applicationId
        minSdkVersion(ConfigData.minSdkVersion)
        targetSdkVersion(ConfigData.targetSdkVersion)
        versionCode = ConfigData.versionCode
        versionName = ConfigData.versionName

        testInstrumentationRunner = ConfigData.testInstrumentationRunner
    }

    buildTypes {
        getByName("release") {
            isMinifyEnabled = false
            proguardFiles(
                getDefaultProguardFile("proguard-android-optimize.txt"),
                "proguard-rules.pro"
            )
        }
    }

    buildFeatures {
        viewBinding = true
    }

    compileOptions {
        sourceCompatibility = JavaVersion.VERSION_1_8
        targetCompatibility = JavaVersion.VERSION_1_8
    }

    kotlinOptions {
        jvmTarget = "1.8"
    }
}

dependencies {

    implementation(RegularDeps.kotlinLib)
    implementation(RegularDeps.coreKtx)
    implementation(RegularDeps.appCompat)
    implementation(RegularDeps.material)
    implementation(RegularDeps.constraintlayout)

    implementation(RegularDeps.mvvm)
    implementation(RegularDeps.mvvmKtx)

    implementation(RegularDeps.rxJava)
    implementation(RegularDeps.rxAndroid)

    implementation(RegularDeps.navFragment)
    implementation(RegularDeps.navUi)

    implementation(RegularDeps.room)
    implementation(RegularDeps.roomKtx)
    implementation(RegularDeps.roomRx)
    kapt(RegularDeps.roomCompiler)

    implementation(RegularDeps.hilt)
    kapt(RegularDeps.hiltCompiler)

    testImplementation(TestDeps.jUnit)
    androidTestImplementation(TestDeps.jUnitExt)
    androidTestImplementation(TestDeps.espresso)
}