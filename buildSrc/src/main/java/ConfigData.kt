object ConfigData {
    const val applicationId = "com.mirkhusainov.alfred"
    const val compileSdkVersion = 31
    const val buildToolsVersion = "30.0.1"
    const val minSdkVersion = 26
    const val targetSdkVersion = 31
    const val versionCode = 1
    const val versionName = "1.0"
    const val testInstrumentationRunner = "androidx.test.runner.AndroidJUnitRunner"
}