object Plugins {
    val gradle by lazy { "com.android.tools.build:gradle:${Versions.pluginGradle}" }
    val kotlin by lazy { "org.jetbrains.kotlin:kotlin-gradle-plugin:${Versions.pluginKotlin}" }
    val safeArgs by lazy { "androidx.navigation:navigation-safe-args-gradle-plugin:${Versions.pluginSafeArgs}" }
    val hilt by lazy { "com.google.dagger:hilt-android-gradle-plugin:${Versions.pluginHilt}" }
}

object RegularDeps {
    val rxJava by lazy { "io.reactivex.rxjava3:rxjava:${Versions.rxJava}" }
    val rxAndroid by lazy { "io.reactivex.rxjava3:rxandroid:${Versions.rxJava}" }

    val navFragment by lazy { "androidx.navigation:navigation-fragment-ktx:${Versions.navigation}" }
    val navUi by lazy { "androidx.navigation:navigation-ui-ktx:${Versions.navigation}" }

    val kotlinLib by lazy { "org.jetbrains.kotlin:kotlin-stdlib:${Versions.kotlinLib}" }
    val coreKtx by lazy { "androidx.core:core-ktx:${Versions.coreKtx}" }
    val appCompat by lazy { "androidx.appcompat:appcompat:${Versions.appCompat}" }
    val mvvm by lazy { "androidx.lifecycle:lifecycle-viewmodel:${Versions.lifecycle}" }
    val mvvmKtx by lazy { "androidx.lifecycle:lifecycle-viewmodel-ktx:${Versions.lifecycle}" }

    //UI
    val material by lazy { "com.google.android.material:material:${Versions.material}" }
    val constraintlayout by lazy { "androidx.constraintlayout:constraintlayout:${Versions.constraintLayout}" }

    //Room
    val room by lazy { "androidx.room:room-runtime:${Versions.room}" }
    val roomRx by lazy { "androidx.room:room-rxjava3:${Versions.room}" }
    val roomCompiler by lazy { "androidx.room:room-compiler:${Versions.room}" }
    val roomKtx by lazy { "androidx.room:room-ktx:${Versions.room}" }

    //Hilt
    val hilt by lazy { "com.google.dagger:hilt-android:${Versions.hilt}" }
    val hiltCompiler by lazy { "com.google.dagger:hilt-android-compiler:${Versions.hilt}" }
}

object TestDeps {
    val jUnit by lazy { "junit:junit:${Versions.jUnit}" }
    val jUnitExt by lazy { "androidx.test.ext:junit:${Versions.jUnitExt}" }
    val espresso by lazy { "androidx.test.espresso:espresso-core:${Versions.espresso}" }
}