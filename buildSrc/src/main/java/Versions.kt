object Versions {
    //Plugins
    const val pluginGradle = "4.1.3"
    const val pluginKotlin = "1.6.10"
    const val pluginSafeArgs = "2.3.5"
    const val pluginHilt = "2.38.1"

    //Regular deps
    const val rxJava = "3.0.0"
    const val navigation = "2.3.5"
    const val appCompat = "1.3.1"
    const val material = "1.4.0"
    const val constraintLayout = "2.1.1"
    const val kotlinLib = "1.5.10"
    const val coreKtx = "1.6.0"
    const val lifecycle = "2.4.0"
    const val room = "2.4.0"
    const val hilt = "2.38.1"

    //Test deps
    const val jUnit = "4.13.2"
    const val jUnitExt = "1.1.3"
    const val espresso = "3.4.0"
}